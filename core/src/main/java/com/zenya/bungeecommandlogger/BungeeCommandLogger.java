package com.zenya.bungeecommandlogger;

import com.zenya.bungeecommandlogger.event.Listeners;
import com.zenya.bungeecommandlogger.util.DiscordHook;
import net.dv8tion.jda.api.JDA;
import net.md_5.bungee.api.plugin.Plugin;

public class BungeeCommandLogger extends Plugin {
    private static BungeeCommandLogger instance;
    private static JDA jda;

    public void onEnable() {
        instance = this;

        jda = DiscordHook.getJda();
        getProxy().getPluginManager().registerListener(this, new Listeners());
    }

    public void onDisable() {
        jda.shutdownNow();
    }

    public static BungeeCommandLogger getInstance() {
        return instance;
    }
}
