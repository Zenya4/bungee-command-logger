package com.zenya.bungeecommandlogger.util;

import com.zenya.bungeecommandlogger.BungeeCommandLogger;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import net.md_5.bungee.config.Configuration;

import javax.security.auth.login.LoginException;
import java.awt.*;

public class DiscordHook {
    private static DiscordHook instance;
    private static JDA jda;
    private static Configuration config = ConfigManager.getInstance().getConfig();

    private DiscordHook() {
        getJda();
    }


    public void sendEmbed(String type, String punisher, String offender, String duration, String reason, Color color) {
        TextChannel channel = jda.getTextChannelById(config.getLong("punishments-channel"));
        String server = BungeeCommandLogger.getInstance().getProxy().getPlayer(punisher).getServer().getInfo().getName();

        EmbedBuilder embed = new EmbedBuilder();
        embed.setTitle(type + " Report");
        embed.setColor(color);
        embed.addField("Executed By", punisher, true);
        embed.addField("Offender", offender, true);
        embed.addField("Duration", duration, true);
        embed.addField("Reason", reason, false);
        embed.setFooter("Server: " + server.replaceFirst(Character.toString(server.charAt(0)), Character.toString(server.charAt(0)).toUpperCase()) + " | Logging by Zenya#0093");

        channel.sendMessage(embed.build()).queue();
    }

    public void sendMessage(String msg, String user) {
        TextChannel channel = jda.getTextChannelById(config.getLong("logging-channel"));
        String server = BungeeCommandLogger.getInstance().getProxy().getPlayer(user).getServer().getInfo().getName();

        EmbedBuilder embed = new EmbedBuilder();
        embed.setTitle(user);
        embed.setDescription(msg);
        embed.setFooter("Server: " + server.replaceFirst(Character.toString(server.charAt(0)), Character.toString(server.charAt(0)).toUpperCase()) + " | Logging by Zenya#0093");
        embed.setColor(Color.CYAN);

        channel.sendMessage(embed.build()).queue();
    }

    public static JDA getJda() {
        if(jda == null) {
            try {
                jda = JDABuilder.createDefault(config.getString("bot-token")).build();
            } catch (LoginException e) {
                e.printStackTrace();
            }
        }
        return jda;
    }

    public static DiscordHook getInstance() {
        if(instance == null) {
            instance = new DiscordHook();
        }
        return instance;
    }
}

