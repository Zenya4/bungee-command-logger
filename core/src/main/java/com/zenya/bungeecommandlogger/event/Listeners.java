package com.zenya.bungeecommandlogger.event;

import com.zenya.bungeecommandlogger.util.CommandParser;
import com.zenya.bungeecommandlogger.util.ConfigManager;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class Listeners implements Listener {

    @EventHandler
    public void onChatEvent(ChatEvent e) {
        if(!e.isCommand()) return;
        if(!(e.getSender() instanceof ProxiedPlayer)) return;

        ProxiedPlayer player = (ProxiedPlayer) e.getSender();

        if(!player.hasPermission(ConfigManager.getInstance().getConfig().getString("log-permission"))) return; //Hacky way to exclude non-staff

        CommandParser.getInstance().parseCommand(player, e.getMessage());
    }
}
