package com.zenya.bungeecommandlogger.util;

import com.zenya.bungeecommandlogger.BungeeCommandLogger;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class ConfigManager {
    private int configVersion = 2; //Change this when updating config

    private static ConfigManager configManager;
    private Plugin plugin = BungeeCommandLogger.getInstance();
    private File configFile = new File(plugin.getDataFolder(), "config.yml");
    private Configuration origConfig = ConfigurationProvider.getProvider(YamlConfiguration.class).load(this.getClass().getClassLoader().getResourceAsStream("config.yml"));
    private Configuration config;

    {
        try {
            config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
        } catch (IOException e) {
            config = origConfig;
        }
    }

    public ConfigManager() {
        if(!getConfigExists()) {
            saveDefaultConfig();
        }

        if(getConfigVersion() != configVersion) {
            File oldConfigFile = new File(plugin.getDataFolder(), "config.yml.v" + String.valueOf(getConfigVersion()));
            try {
                Files.copy(configFile.toPath(), oldConfigFile.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }

            configFile.delete();
            saveDefaultConfig();
        }

    }

    private boolean getConfigExists() {
        return configFile.exists();
    }

    private int getConfigVersion() {
        return config.getInt("config-version");
    }

    public void saveConfig() {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(config, configFile);
        } catch (IOException e) {

        }
    }

    public void saveDefaultConfig() {
        if (!plugin.getDataFolder().exists())
            plugin.getDataFolder().mkdir();

        if (!configFile.exists()) {
            try {
                Files.copy(this.getClass().getClassLoader().getResourceAsStream("config.yml"), configFile.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Configuration getConfig() {
            return config;
    }

    public static ConfigManager getInstance() {
        if(configManager == null) {
            configManager = new ConfigManager();
        }
        return configManager;
    }
}
