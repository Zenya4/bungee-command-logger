package com.zenya.bungeecommandlogger.util;

import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

public class CommandParser {
    private static CommandParser parser;

    public void parseCommand(ProxiedPlayer player, String message) {
        ArrayList<String> args = new ArrayList<String>();
        ArrayList<String> command = new ArrayList<String>();
        message = message.replaceFirst("/", "");

        command.addAll(Arrays.asList(message.split(" ")));

        //Ethical reasons :)
        ArrayList<String> unloggedCommands = new ArrayList<String>();
        unloggedCommands.add("authme");
        unloggedCommands.add("login");
        unloggedCommands.add("register");
        for(String check : unloggedCommands) {
            if(command.get(0).toLowerCase().startsWith(check)) {
                return;
            }
        }

        //Handle logging commands (messages)
        for(Object check : ConfigManager.getInstance().getConfig().getList("logged-commands")) {
            if(command.get(0).toLowerCase().startsWith(String.valueOf(check).toLowerCase())) {
                DiscordHook.getInstance().sendMessage("/" + message, player.getName());
                return;
            }
        }

        //Handle punishments (embed)
        if(message.contains("-s")) {
            return;
        }

        if(command.get(0).toUpperCase().equals("CMI")) {
            command.remove(0);
        }

        if(command == null || command.size() == 0) {
            return;
        }

        String cmiCommand = command.get(0);
        command.remove(0);

        if(command == null || command.size() == 0) {
            return;
        }

        args = command;

        String punisher = player.getName();
        String type = "Unspecified";
        String duration = "N/A";
        String reason = "None";
        Color color = Color.BLACK;

        String offender = args.get(0);
        args.remove(0);

        switch(cmiCommand.toUpperCase()) {
            case "BAN":
                type = "Ban";
                duration = "∞";
                color = Color.RED;

                if(args == null || args.size() == 0) {
                    break;
                }
                reason = "";
                for(String str : args) {
                    reason = reason + str + " ";
                }
                break;

            case "TEMPBAN":
                type = "Tempban";
                color = Color.RED;

                if(args == null || args.size() == 0) {
                    break;
                }

                duration = args.get(0);
                args.remove(0);

                if(args == null || args.size() == 0) {
                    break;
                }

                reason = "";
                for(String str : args) {
                    reason = reason + str + " ";
                }
                break;

            case "MUTE":
                type = "Mute";
                duration = "∞";
                color = Color.ORANGE;

                if(args == null || args.size() == 0) {
                    break;
                }

                duration = args.get(0);
                args.remove(0);

                if(args == null || args.size() == 0) {
                    break;
                }

                reason = "";
                for(String str : args) {
                    reason = reason + str + " ";
                }
                break;

            case "JAIL":
                type = "Jail";
                duration = "5m";
                color = Color.ORANGE;

                if(args == null || args.size() == 0) {
                    break;
                }

                duration = args.get(0);
                args.remove(0);

                try {
                    args.remove(0); // jailName
                    args.remove(0); // cellID
                } catch(IndexOutOfBoundsException | NullPointerException e) {
                    //Do nothing
            }

                if(args == null || args.size() == 0) {
                    break;
                }

                reason = "";
                for(String str : args) {
                    str = str.replaceAll("r:", "");
                    reason = reason + str + " ";
                }
                break;

            case "KICK":
                type = "Kick";
                color = Color.YELLOW;

                if(args == null || args.size() == 0) {
                    break;
                }
                reason = "";
                for(String str : args) {
                    reason = reason + str + " ";
                }
                break;

            case "UNBAN":
                type = "Unban";
                color = Color.GREEN;
                break;

            case "UNMUTE":
                type = "Unmute";
                color = Color.CYAN;
                break;

            case "UNJAIL":
                type = "Unjail";
                color = Color.CYAN;
                break;

            default:
                return;
        }
        DiscordHook.getInstance().sendEmbed(type, punisher, offender, duration, reason, color);
    }

    public static CommandParser getInstance() {
        if(parser == null) {
            parser = new CommandParser();
        }
        return parser;
    }
}
